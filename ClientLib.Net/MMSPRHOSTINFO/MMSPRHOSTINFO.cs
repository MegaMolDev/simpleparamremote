﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaMol.SimpleParamRemote.MMSPRHOSTINFO {

    /// <summary>
    /// Answers the native OS process id of the host
    /// </summary>
    public sealed class GETPROCESSID : Request {
        public override string Command { get { return "GETPROCESSID"; } }
        protected override string[] parameters { get { return null; } }
        internal override object parseAnswerFromZString(string v) {
            return UInt32.Parse(v);
        }
    }

}
