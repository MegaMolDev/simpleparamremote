﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaMol.SimpleParamRemote.MMSPR2 {

    /// <summary>
    /// Answers all info about a specific module
    /// </summary>
    public sealed class GETMODULEPARAMS : Request {
        public override string Command { get { return "GETMODULEPARAMS"; } }
        /// <summary>
        /// Gets or sets the name of the parameter slot to query
        /// </summary>
        public string ParameterName { get; set; }
        protected override string[] parameters { get { return new string[] { ParameterName }; } }

        /// <remarks>
        /// http://stackoverflow.com/a/321404/552373
        /// </remarks>
        private static byte[] StringToByteArray(string hex) {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
        internal override object parseAnswerFromZString(string v) {
            //string[] content = v.Split(new char[] { '\u0001' }, StringSplitOptions.None);

            //int len = content.Count();
            //// don't forget the trailing element since we conclude with one last separator!
            //if (len > 1 && (len - 1) % 4 == 1) {
            //    StringBuilder sb = new StringBuilder();
            //    sb.Append(content[0] + System.Environment.NewLine);
            //    for (int x = 1; x < len - 1; x += 4) {
            //        sb.Append(content[x + 0] + System.Environment.NewLine);
            //        sb.Append(content[x + 1] + System.Environment.NewLine);

            //        byte[] dat = StringToByteArray(content[x + 2]);
            //        ParameterTypeDescription d = new ParameterTypeDescription();
            //        d.SetFromBinDescription(dat);
            //        sb.Append(d.Type.ToString() + System.Environment.NewLine);

            //        sb.Append(content[x + 3] + System.Environment.NewLine);
            //    }
            //    return sb.ToString();
            //} else {
            //    return "cannot parse the answer";
            //}
            return v;

        }
    }

}
