#pragma once

#include "CommandFunctionPtr.h"
#include "mmcore/AbstractNamedObjectContainer.h"
#include <map>

namespace megamol {
namespace SimpleParamRemote {

    class ProtocolSimpleParamRemote1 {
    public:
        ProtocolSimpleParamRemote1() = delete;
        ~ProtocolSimpleParamRemote1() = delete;

        static const char* ProtocolName() { return "MMSPR1"; }
        static CommandMapType GetCommands();

    private:

        static std::string answerParamTree(ModuleGraphAccess& mgAccess, const std::string& param);
        static void answerParamTree(std::stringstream& reply, core::AbstractNamedObjectContainer::const_ptr_type anoc);
        static std::string getParamType(ModuleGraphAccess& mgAccess, const std::string& param);
        static std::string getParamDesc(ModuleGraphAccess& mgAccess, const std::string& param);
        static std::string getParamValue(ModuleGraphAccess& mgAccess, const std::string& param);
        static std::string setParamValue(ModuleGraphAccess& mgAccess, const std::string& param);

    };

}
}
