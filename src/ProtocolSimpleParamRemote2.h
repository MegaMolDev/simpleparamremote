#pragma once

#include "CommandFunctionPtr.h"
#include "mmcore/AbstractNamedObjectContainer.h"
#include <map>

namespace megamol {
namespace SimpleParamRemote {

    class ProtocolSimpleParamRemote2 {
    public:
        ProtocolSimpleParamRemote2() = delete;
        ~ProtocolSimpleParamRemote2() = delete;

        static const char* ProtocolName() { return "MMSPR2"; }
        static CommandMapType GetCommands();

    private:

        static std::string answerModuleParams(ModuleGraphAccess& mgAccess, const std::string& param);
    };

}
}
