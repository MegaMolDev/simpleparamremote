#pragma once

#include "CommandFunctionPtr.h"
#include "mmcore/AbstractNamedObjectContainer.h"
#include <map>

namespace megamol {
namespace SimpleParamRemote {

    class ProtocolHostInfo {
    public:
        ProtocolHostInfo() = delete;
        ~ProtocolHostInfo() = delete;

        static const char* ProtocolName() { return "MMSPRHOSTINFO"; }
        static CommandMapType GetCommands();

    private:

        static std::string getProcessID(ModuleGraphAccess& mgAccess, const std::string& param);

    };

}
}
