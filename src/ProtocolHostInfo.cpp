#include "stdafx.h"
#include "ProtocolHostInfo.h"
#include "vislib/sys/Process.h"

using namespace megamol;

std::map<std::string, SimpleParamRemote::CommandFunctionPtr> SimpleParamRemote::ProtocolHostInfo::GetCommands() {
    CommandMapType rv;

    rv["GETPROCESSID"] = &ProtocolHostInfo::getProcessID;

    return rv;
}

std::string SimpleParamRemote::ProtocolHostInfo::getProcessID(ModuleGraphAccess& mgAccess, const std::string& param) {
    vislib::StringA str;
    unsigned int id = vislib::sys::Process::CurrentID();
    str.Format("%u", id);
    return std::string(str.PeekBuffer());
}
