#include "stdafx.h"
#include "ZMQContextUser.h"

std::weak_ptr<megamol::SimpleParamRemote::ZMQContextUser> megamol::SimpleParamRemote::ZMQContextUser::inst;

megamol::SimpleParamRemote::ZMQContextUser::ptr megamol::SimpleParamRemote::ZMQContextUser::Instance() {
    ptr p = inst.lock();
    if (!p) {
        inst = p = ptr(new ZMQContextUser());
    }
    return p;
}

megamol::SimpleParamRemote::ZMQContextUser::ZMQContextUser()
    : context(1) {
}

megamol::SimpleParamRemote::ZMQContextUser::~ZMQContextUser() {
}
