#include "stdafx.h"
#include "ProtocolSimpleParamRemote2.h"
#include "vislib/sys/AutoLock.h"
#include <sstream>
#include <cstdint>
#include "mmcore/CoreInstance.h"
#include "mmcore/Module.h"
#include "mmcore/param/ParamSlot.h"
#include "vislib/UTF8Encoder.h"

using namespace megamol;
using namespace megamol::core;
using megamol::core::AbstractNamedObject;
using megamol::core::AbstractNamedObjectContainer;

SimpleParamRemote::CommandMapType SimpleParamRemote::ProtocolSimpleParamRemote2::GetCommands() {
    CommandMapType rv;

    rv["GETMODULEPARAMS"] = &ProtocolSimpleParamRemote2::answerModuleParams;
    return rv;
}

std::string SimpleParamRemote::ProtocolSimpleParamRemote2::answerModuleParams(ModuleGraphAccess& mgAccess, const std::string& param) {
    vislib::sys::AutoLock l(mgAccess.ModuleGraphLock());

    AbstractNamedObject::const_ptr_type ano = mgAccess.RootModule();
    AbstractNamedObjectContainer::const_ptr_type anoc = std::dynamic_pointer_cast<const AbstractNamedObjectContainer>(ano);
    if (!anoc) return "ERR QUERYBROKEN no root";
    Module::const_ptr_type mod = Module::dynamic_pointer_cast(const_cast<AbstractNamedObjectContainer*>(anoc.get())->FindNamedObject(param.c_str()));
    if (!mod) return "ERR NOTFOUND module not found";

    std::stringstream answer;
    vislib::StringA name(mod->FullName());
    answer << name << "\1";
    AbstractNamedObjectContainer::child_list_type::const_iterator si, se;
    se = mod->ChildList_End();
    for (si = mod->ChildList_Begin(); si != se; ++si) {
        const param::ParamSlot *slot = dynamic_cast<const param::ParamSlot*>((*si).get());
        if (slot != NULL) {
            //name.Append("::");
            //name.Append(slot->Name());
            
            answer << slot->Name() << "\1";

            vislib::StringA descUTF8;
            vislib::UTF8Encoder::Encode(descUTF8, slot->Description());
            answer << descUTF8 << "\1";

            auto psp = slot->Parameter();
            if (psp.IsNull()) return "ERR NOTFOUND ParamSlot does seem to hold no parameter";

            vislib::RawStorage pspdef;
            psp->Definition(pspdef);
            // not nice, but we make HEX (base64 would be better, but I don't care)
            std::string answer2(pspdef.GetSize() * 2, ' ');
            for (SIZE_T i = 0; i < pspdef.GetSize(); ++i) {
                uint8_t b = *pspdef.AsAt<uint8_t>(i);
                uint8_t bh[2] = { static_cast<uint8_t>(b / 16), static_cast<uint8_t>(b % 16) };
                for (unsigned int j = 0; j < 2; ++j) answer2[i * 2 + j] = (bh[j] < 10u) ? ('0' + bh[j]) : ('A' + (bh[j] - 10u));
            }
            answer << answer2 << "\1";

            vislib::StringA valUTF8;
            vislib::UTF8Encoder::Encode(valUTF8, psp->ValueString());

            answer << valUTF8 << "\1";
        }
    }

    return answer.str();
}
