#pragma once

#include "mmcore/AbstractService.h"
#include "ModuleGraphAccess.h"
#include "mmcore/AbstractNamedObject.h"
#include "mmcore/AbstractNamedObjectContainer.h"
#include <thread>
#include <zmq.hpp>
#include "ZMQContextUser.h"
#include <string>
#include "CommandFunctionPtr.h"
#include <map>

namespace megamol {
namespace SimpleParamRemote {

    class HostService : public core::AbstractService {
    public:

        static unsigned int ID;

        virtual const char* Name() const { return "SimpleParamRemote"; }

        HostService(core::CoreInstance& core);
        virtual ~HostService();

        virtual bool Initalize(bool& autoEnable);
        virtual bool Deinitialize();

        inline const std::string& GetAddress(void) const {
            return address;
        }
        void SetAddress(const std::string& ad);

    protected:
        virtual bool enableImpl();
        virtual bool disableImpl();

    private:

        static std::string protocols;
        static std::string getProtocols(ModuleGraphAccess& mgAccess, const std::string& param);

        void serve();
        std::string makeAnswer(const std::string& req);

        ModuleGraphAccess mgAccess;
        ZMQContextUser::ptr context;

        std::thread serverThread;
        bool serverRunning;

        std::string address;

        CommandMapType commandMap;

    };


}
}
