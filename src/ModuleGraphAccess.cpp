#include "stdafx.h"
#include "ModuleGraphAccess.h"
#include "mmcore/CoreInstance.h"

using namespace megamol;

SimpleParamRemote::ModuleGraphAccess::ModuleGraphAccess(core::CoreInstance& core) : core(core) {
}

SimpleParamRemote::ModuleGraphAccess::~ModuleGraphAccess() {
}

core::AbstractNamedObject::const_ptr_type SimpleParamRemote::ModuleGraphAccess::RootModule() {
    return core.ModuleGraphRoot();
}
