#include "stdafx.h"
#include "HostSettingsModule.h"
#include "mmcore/param/StringParam.h"
#include "mmcore/param/BoolParam.h"
#include <iostream>
#include "vislib/sys/Log.h"
#include <cassert>
#include "vislib/sys/AutoLock.h"
#include <sstream>
#include "vislib/UTF8Encoder.h"
#include "mmcore/CoreInstance.h"

using namespace megamol;

SimpleParamRemote::HostSettingsModule::HostSettingsModule()
        : core::Module(),
        portSlot("port", "The local port number to open a socket on for listening for incoming connections"),
        enabledSlot("enabled", "Enables/disables remote parameter control") {

    portSlot.SetParameter(new core::param::StringParam("tcp://*:35421")); // bind string format for ZeroMQ
    portSlot.SetUpdateCallback(&HostSettingsModule::portSlotChanged);
    MakeSlotAvailable(&portSlot);

    enabledSlot.SetParameter(new core::param::BoolParam(true));
    enabledSlot.SetUpdateCallback(&HostSettingsModule::enabledSlotChanged);
    MakeSlotAvailable(&enabledSlot);

}

SimpleParamRemote::HostSettingsModule::~HostSettingsModule() {
    Release();
}

bool SimpleParamRemote::HostSettingsModule::create() {
    HostService* ser = getHostService();
    if (ser == nullptr) return false;

    portSlot.Param<core::param::StringParam>()->SetValue(ser->GetAddress().c_str(), false);
    enabledSlot.Param<core::param::BoolParam>()->SetValue(ser->IsEnabled(), false);

    return true;
}

void SimpleParamRemote::HostSettingsModule::release() {
    // intentionally empty
}

SimpleParamRemote::HostService *SimpleParamRemote::HostSettingsModule::getHostService() {
    core::AbstractService* s = GetCoreInstance()->GetInstalledService(HostService::ID);
    return dynamic_cast<HostService*>(s);
}

bool SimpleParamRemote::HostSettingsModule::portSlotChanged(core::param::ParamSlot& slot) {
    if (&slot != &portSlot) return false;
    HostService* ser = getHostService();
    if (ser == nullptr) return false;

    ser->SetAddress(std::string(vislib::StringA(portSlot.Param<core::param::StringParam>()->Value()).PeekBuffer()));

    return true;
}

bool SimpleParamRemote::HostSettingsModule::enabledSlotChanged(core::param::ParamSlot& slot) {
    if (&slot != &enabledSlot) return false;
    HostService* ser = getHostService();
    if (ser == nullptr) return false;

    if (enabledSlot.Param<core::param::BoolParam>()->Value()) {
        ser->Enable();
    } else {
        ser->Disable();
    }

    return true;
}
