#pragma once

#include "mmcore/Module.h"
#include "mmcore/param/ParamSlot.h"
#include "HostService.h"

namespace megamol {
namespace SimpleParamRemote {

    class HostSettingsModule : public core::Module {
    public:
        static const char * ClassName() { return "SimpleParamRemoteHost"; }
        static const char * Description() { return "Host Modules for SimpleParamRemote"; }
        static bool IsAvailable() { return true; }

        HostSettingsModule();
        virtual ~HostSettingsModule();

    protected:
        virtual bool create(void);
        virtual void release(void);

    private:
        HostService *getHostService();

        bool portSlotChanged(core::param::ParamSlot& slot);
        bool enabledSlotChanged(core::param::ParamSlot& slot);

        core::param::ParamSlot portSlot;
        core::param::ParamSlot enabledSlot;

    };


}
}
